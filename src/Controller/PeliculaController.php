<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFromType;
use App\Manager\PeliculasManager;
use App\Manager\UsuarioManager;
use App\Security\LoginAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use App\Repository\PeliculaRepository;

class PeliculaController extends AbstractController
{

    /**
     * @Route("/", name="general-info")
     * 
     */
    public function generalInfo(PeliculasManager $peliculasManager, EntityManagerInterface $em)
    {
        $peliculas = $peliculasManager->getPeliculas($em);

        return $this->render(
            'general-info.html.twig'
        );
    }
    /**
     * @Route("/list", name="homepage")
     *
     */
    public function listAll(PeliculasManager $peliculasManager, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $peliculas = $peliculasManager->getPeliculas($em);

        return $this->render(
            'list-peliculas.html.twig',
            [
                'peliculas' => $peliculas
            ]
        );
    }

    /**
     * @Route("/movies/{id}", name="app_details_pelicula")
     * 
     */
    public function details($id, PeliculasManager $peliculasManager, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $pelicula = $peliculasManager->getMovie($em,$id);

        if (!isset($pelicula)) {
            return new Response("Pelicula no encontrada", 404);
        }

        return $this->render(
            'details-pelicula.html.twig',
            [
                'pelicula' => $pelicula
            ]
        );
    }

    /**
     * @Route("/movies", name="list-movies")
     * 
     */
    public function listMovies(PeliculasManager $peliculasManager, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $peliculas = $peliculasManager->getMovies($em);

        return $this->render(
            'list-peliculas.html.twig',
            [
                'peliculas' => $peliculas
            ]
        );
    }

    /**
     * @Route("/series", name="list-series")
     * 
     */
    public function listSeries(PeliculasManager $peliculasManager, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $peliculas = $peliculasManager->getSeries($em);

        return $this->render(
            'list-peliculas.html.twig',
            [
                'peliculas' => $peliculas
            ]
        );
    }

     /**
     * @Route("/account", name="account")
     * 
     */
    public function account(Request $request,GuardAuthenticatorHandler $guard,UserPasswordEncoderInterface $passwordEncoder, LoginAuthenticator $formAuthenticator, EntityManagerInterface $em, UsuarioManager $userManager)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $formRegistration = $this->createForm(RegistrationFromType::class, $user);
        $formRegistration->handleRequest($request);

        if ($formRegistration->isSubmitted() && $formRegistration->isValid()) {

            $user = $formRegistration->getData();
            //si existe ya ese email debe dar error
            $check = $userManager->getUserByEmail($em, $request->request->get('email'));
            if(isset($check)){
                $this->addFlash('error', 'Email already in use.');
            }else{
                $user->setPassword($passwordEncoder->encodePassword($user,
                    $user->getPassword()));
                $em->flush();
                return $this->redirectToRoute('account');
            }

        }
        return $this->render('account-details.html.twig', [
            'formUser' => $formRegistration->createView()
        ]);
    }

    
     /**
     * @Route("/search/", name="search")
     * 
     */
    public function search(PeliculaRepository $repo, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $value = $request->query->get('searchValue');
        $peliculas = $repo->findByField($value);
        return $this->render(
            'list-peliculas.html.twig',
            [
                'peliculas' => $peliculas
            ]
        );
    }

}
