<?php

namespace App\Repository;

use App\Entity\Pelicula;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pelicula|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pelicula|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pelicula[]    findAll()
 * @method Pelicula[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeliculaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pelicula::class);
    }

    /**
    * @return Pelicula[] Returns an array of Pelicula objects
    */
    public function findByField($value)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb->where(
                $qb->expr()->like('p.title',':value')
            )
            ->orWhere(
                $qb->expr()->like('p.plot',':value')
            )
            ->orWhere(
                $qb->expr()->like('p.genre',':value')
            )
            ->orWhere(
                $qb->expr()->like('p.Director',':value')
            )
            ->orWhere(
                $qb->expr()->like('p.writer',':value')
            )
            ->orWhere(
                $qb->expr()->like('p.actors',':value')
            )
            ->setParameter('value', '%'.$value.'%')
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?Pelicula
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
