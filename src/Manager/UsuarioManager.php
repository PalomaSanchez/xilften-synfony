<?php

namespace App\Manager;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UsuarioManager{
    public function saveUser(User $user, EntityManagerInterface $em)
    {
        $em->persist($user);
        $em->flush();
    }

    public function getUserInfo(EntityManagerInterface $em, $idUser): User
    {
        $repositorio = $em->getRepository(User::class);
        $user = $repositorio->find($idUser);

        return $user;
    }

    public function getUserByEmail(EntityManagerInterface $em, $email){
        $repositorio = $em->getRepository(User::class);
        $user = $repositorio->findOneBy(['email'=>$email]);

        return $user;
    }
}