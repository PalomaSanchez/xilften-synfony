<?php

namespace App\Manager;

use App\Entity\Pelicula;
use Doctrine\ORM\EntityManagerInterface;

class PeliculasManager
{

    public function __construct()
    {
    }

    public function savePelicula(Pelicula $pelicula, EntityManagerInterface $em)
    {
        $em->persist($pelicula);
        $em->flush();
    }

    public function getPeliculas(EntityManagerInterface $em): array
    {
        $repositorio = $em->getRepository(Pelicula::class);
        $peliculas = $repositorio->findAll();

        return $peliculas;
    }

    public function getMovies(EntityManagerInterface $em): array
    {
        $repositorio = $em->getRepository(Pelicula::class);
        $peliculas = $repositorio->findBy(['type'=>'movie']);

        return $peliculas;
    }

    public function getSeries(EntityManagerInterface $em): array
    {
        $repositorio = $em->getRepository(Pelicula::class);
        $peliculas = $repositorio->findBy(['type'=>'series']);

        return $peliculas;
    }

    public function getMovie(EntityManagerInterface $em, $idPeli): Pelicula
    {
        $repositorio = $em->getRepository(Pelicula::class);
        $pelicula = $repositorio->find($idPeli);

        return $pelicula;
    }


}
